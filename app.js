var restify = require("restify");
var mongojs = require("mongojs");
var https = require("https");

//var ip_addr = "http://fierce-reef-9512.herokuapp.com";
//var ip_addr = "127.0.0.1";
var ip_addr = "127.0.0.1";
var port = process.env.PORT || 8080;

var server = restify.createServer({
    name: "myapp"
    
});

server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());

// configures mongodb
var connection_string="mongodb://heroku_app22245983:p8c9pmi38oitb844v2g18rsre8@ds027829.mongolab.com:27829/heroku_app22245983";
var db = mongojs(connection_string, ["test"]);
var users = db.collection("users");

var PATH = "/users"

server.get({path:PATH, version: "0.0.1"}, findMatches);
server.get({path:PATH+"/:userId", version: "0.0.1"}, findUser);
server.put({path:PATH+"/edit", version: "0.0.1"}, editUser);
server.post({path:PATH, version: "0.0.1"}, postNewUser);
server.put({path:PATH + "/message", version: "0.0.1"}, postMessage);
server.del({path:PATH+"/:userId", version: "0.0.1"}, deleteUser);
server.put({path:PATH, version: "0.0.1"}, verifyUser);


server.listen(port, function() {
    console.log("%s listening at %s ", server.name, server.url);
});

function postMessage(req, res, next) {
    console.log("I am in post message");
    var message = {};
    message.username = req.params.username;
    message.response = req.params.response;
    console.log(message);
    users.update({"username":req.params.username},
		 {$push: { "messages": message}},
		 function(err, success) {
		     if (success) {
			 res.send(201, success);
			 console.log("sucessfully posted messages");
			 return next();
		     } else {
			 return next(err);
		     }
		 });
   
}

function findAllUsers(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    // finish implementing
    console.log("No yet finished");
}

function findMatches(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    var interested_in = req.params.interested_in;
    console.log("prev interested_In");
    console.log(interested_in);
    var interested_in_array = interested_in.split(",");
    console.log("I am in terested in?");
    console.log(interested_in_array);
    users.find({"skills":{$in: interested_in_array}, "state": req.params.state, "city":req.params.city}, {limit: 5},
	       function(err, success) {
		   if (success) {
		       res.send(200, success);
		       console.log("i am in find matches");
		       console.log(success);
		       return next();
		   }
		   return next(err);
	       });
}

function findUser(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    users.findOne({_id:mongojs.ObjectId(req.params.userId)},
		  function(err, success) {
		      console.log("Response success " + success);
		      console.log("Response error " + err);
		      if (success) {
			  res.send(200, success);
			  return next();
		      }
		      return next(err);
		  })
}

    

function postNewUser(req, res, next) {
    // verify token
    console.log("i am in postnew user");
    https.get("https://www.googleapis.com/oauth2/v1/userinfo?access_token="
	     + req.params.token,
	     function(response) {
		 var bodyChunks = [];
		 response.on("data", function(chunk) {
		     bodyChunks.push(chunk);
		 }).on("end", function() {
		     var body = Buffer.concat(bodyChunks);
		     console.log(body);
		 });
		 

		 if (bodyChunks) {
		     var user = {};
		     user.token = req.params.token;

		     user.email = req.params.email;
		     user.username = req.params.username;
		     user.password = req.params.password;
		     user.city = req.params.city;
		     user.state = req.params.state;
		     var interested_in = req.params.interested_in;
		     user.interested_in = interested_in.split(", ");
		     var skills = req.params.skills;
		     user.skills = skills.split(", ");
		     res.setHeader("Access-Control-Allow-Origin", "*");
		     users.save(user, function(err, success) {
			 console.log("posting new user");
			 console.log("Response success ");
			 console.log(success);
			 console.log("Response error ");
			 console.log(err);
			 if (success) {
			     res.send(201, user);
			     return next();
			 } else {
			     return next(err);
			 }
		     });
		 }
	     }).on("error", function(e) {
		 console.log("error");
	     });
}

function deleteUser(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    users.remove({_id:mongojs.ObjectId(req.params.userId)},
		 function(err, success) {
		     console.log("Response success " + success);
		     console.log("Response error " + err);
		     if (success) {
			 res.send(204);
			 return next();
		     } else {
			 return next(err);
		     }
		 })
}

function verifyUser(req, res, next) {
    res.set("Access-Control-Allow-Origin", "*");
    var username = req.params.username;
    var password = req.params.password;
    console.log("I am in put");
    users.findOne({"username":username, "password":password}, 
		  function(err, success) {
		      if(success) {
			  console.log(success);
			  res.send(200, success);
			  return next();
		      }
		      return next(err);
		  });
    }

function editUser(req, res, next) {
    res.set("Access-Control-Allow-Origin", "*");
    var params = req.params;
    for (var prop in params) {
	console.log(prop);
	var propValue = params[prop];
	var changeVal = {};
	changeVal[prop] = propValue;
	console.log("this is change value");
	console.log(changeVal);
	users.update({"username": req.params.username}, {$set: changeVal},
		     function(err, success) {
			 if (success) {
			 	users.findOne({"username": req.params.username},
					      function(err, success) {
						  if (success) {
						      console.log("finished editing");
						      console.log(success);
						      res.send(200, success);
						      return next();
						  }
						  return next(err);
					      });
			 }});

    }
}





